# Frontend-Workshop

## Prelude

### Setup
Clone the todosy repository, exercises-branch:

```shell
$ git clone -b exercises https://bitbucket.org/tdeekens/todosy.git
```

Install dependencies:
```
$ npm install
$ npm start
```
Open [http://localhost:3000](http://localhost:3000) and login.


### Switch GIT branch to master (and back)
Switch to master branch (with solutions):
```
$ git checkout master
```

... and back to exercises:
```
$ git checkout exercises
```

### Fiddling with JavaScript JSBIN
- [Sebastian's JSBIN]( https://jsbin.com/cetabigore/edit?js,console)
- [Tobias`s JSBIN](https://jsbin.com/mizate/edit?js,console)


# Exercises

## Trash Tasks Use-Case

## Right-Click Undo Use-Case

## Write a test for Undo (api.unassign)

## Fix Action-Controller (Test fails for status message)
