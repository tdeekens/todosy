#! /usr/bin/env bash

PROXY=proxy.inf.epost-dev.de
export http_proxy="http://$PROXY:8080"
export https_proxy="http://$PROXY:8080"
export HTTP_PROXY="http://$PROXY:8080"
export HTTPS_PROXY="http://$PROXY:8080"
export ftp_proxy="ftp://$PROXY:8080"
export socks_proxy="$PROXY:8080"
export no_proxy="10.172.151.38,localhost,127.0.0.0/8,127.0.0.1,nathan.local,*.local,*.epost.de,*.epost-dev.de,review.epost.de,git.epost.de,*.testingepost.de,10.175.16.9,10.175.16.9,01.bis.epost-dev.de,10.172.151.231,10.175.16.9,npm-1.bis.epost-dev.de,10.175.24.53"
