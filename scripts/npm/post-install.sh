#!/bin/bash

webpackcorsdir="node_modules/webpack-dev-server-cors"

# set http proxy for git, git being triggered by bower
export http_proxy=http://proxy.inf.epost-dev.de:8080
export https_proxy=http://proxy.inf.epost-dev.de:8080

echo "### Installing the protractor webdriver..."

node_modules/protractor/bin/webdriver-manager update --standalone

if [ -d "$webpackcorsdir" -a ! -h "$webpackcorsdir" ]
then
   echo "### $webpackcorsdir found - linking it to webpack-dev-server..."

   rm -rf node_modules/.bin/webpack-dev-server
   rm -rf node_modules/.bin/webpack-dev-server-cors
   rm -rf node_modules/webpack-dev-server

   ln -s webpack-dev-server-cors node_modules/webpack-dev-server
   ln -s ../webpack-dev-server-cors/bin/webpack-dev-server.js node_modules/.bin/webpack-dev-server
else
   echo "### $webpackcorsdir not found - no move needed..."
fi

npm run build
