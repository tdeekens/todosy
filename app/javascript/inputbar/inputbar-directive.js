module.exports = {
  ngProvider: 'directive',
  ngModule: 'directives',
  ngName: 'todosyInputbar',
  dependencies: ['todosy.util.template'],
  fn: (templateFactory) => {
    return {
      restrict: 'E',
      controller: 'todosy.inputbar.controller',
      templateUrl: templateFactory.url('inputbar'),
      replace: true,
      scope: {
      },
      link: () => {}
    };
  }
};
