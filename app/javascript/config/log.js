module.exports = {
  ngProvider: 'constant',
  ngModule: 'config',
  ngName: 'todosy.config.log',
  dependencies: [],
  fn: {
    enabled: false
  }
};
