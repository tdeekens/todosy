module.exports = {
  ngProvider: 'constant',
  ngModule: 'config',
  ngName: 'todosy.config.host',
  dependencies: [],
  fn: {
    host: 'browser'
  }
};
