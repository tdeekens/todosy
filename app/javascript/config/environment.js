module.exports = {
  ngProvider: 'constant',
  ngModule: 'config',
  ngName: 'todosy.config.environment',
  dependencies: [],
  fn: {
    root: 'todosy/',
    apiRoot: '',
    apiExtension: '',
    paramsDelimiter: '?',
    paramsGlue: '&',
    pathPostfix: '',
    templateRoot: 'mui/todosy/templates/',
    templateExtension: '.html'
  }
};
