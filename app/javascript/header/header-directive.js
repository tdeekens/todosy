module.exports = {
  ngProvider: 'directive',
  ngModule: 'directives',
  ngName: 'todosyHeader',
  dependencies: ['todosy.util.template'],
  fn: (templateFactory) => {
    return {
      restrict: 'E',
      controller: 'todosy.header.controller',
      templateUrl: templateFactory.url('header'),
      replace: true,
      scope: {
      },
      link: () => {}
    };
  }
};
