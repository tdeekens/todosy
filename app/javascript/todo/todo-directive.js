module.exports = {
  ngProvider: 'directive',
  ngModule: 'directives',
  ngName: 'todosyTodo',
  dependencies: [
    'todosy.util.template'
  ],
  fn: (templateFactory) => {
    return {
      restrict: 'E',
      controller: 'todosy.todo.controller',
      templateUrl: templateFactory.url('todo'),
      replace: true,
      scope: {
        todo: '='
      },
      link: () => {}
    };
  }
};
