module.exports = {
  ngProvider: 'directive',
  ngModule: 'directives',
  ngName: 'todosyAction',
  dependencies: ['todosy.util.template'],
  fn: (templateFactory) => {
    return {
      restrict: 'E',
      controller: 'todosy.action.controller',
      templateUrl: templateFactory.url('action'),
      replace: true
    };
  }
};
