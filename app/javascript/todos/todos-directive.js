module.exports = {
  ngProvider: 'directive',
  ngModule: 'directives',
  ngName: 'todosyTodos',
  dependencies: [
    'todosy.util.template'
  ],
  fn: (templateFactory) => {
    return {
      restrict: 'E',
      controller: 'todosy.todos.controller',
      templateUrl: templateFactory.url('todos'),
      replace: true,
      scope: {
      },
      link: () => {}
    };
  }
};
