module.exports = {
  ngProvider: 'controller',
  ngModule: 'controllers',
  ngName: 'todosy.todos.controller',
  dependencies: [
    '$scope', 'todosy.util.api'
  ],
  fn: ($scope, api) => {
    $scope.todos = api.load();
  }
};
